<?php

namespace Qerana\Security\Model;

interface UserInterface
{
    /**
     * @param mixed $id_user
     */
    public function setIdUser($id_user): void;


    public function createPassword(string $pass):void;

    /**
     * @param mixed $username
     */
    public function setUsername($username): void;

    /**
     * @param mixed $email_user
     */
    public function setEmailUser($email_user): void;

    /**
     * @param mixed $password
     */
    public function setPassword($password): void;

    /**
     * @param mixed $status
     */
    public function setStatus($status): void;

    /**
     * @param mixed $user_type
     */
    public function setUserType($user_type): void;


    public function setRoles(string $roles): void;

    /**
     * @return mixed
     */
    public function getIdUser();

    /**
     * @return mixed
     */
    public function getUsername();

    /**
     * @return mixed
     */
    public function getEmailUser();

    /**
     * @return mixed
     */
    public function getPassword();

    /**
     * @return mixed
     */
    public function getStatus();

    /**
     * @return mixed
     */
    public function getUserType();

    /**
     * @return array
     */
    public function getRoles(): array;
}