<?php

namespace Qerana\Security\Model\Event;

use Qerana\Security\Model\UserInterface;
use Symfony\Contracts\EventDispatcher\Event;

class LoginSuccessEvent extends Event
{


    public const NAME = 'login.success';
    /**
     * @var UserInterface
     */
    private $user;


    public function __construct(UserInterface $user)
    {
        $this->user = $user;
    }


    public function getUser(): UserInterface
    {
        return $this->user;
    }

}