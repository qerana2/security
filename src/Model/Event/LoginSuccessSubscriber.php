<?php

namespace Qerana\Security\Model\Event;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;


class LoginSuccessSubscriber implements EventSubscriberInterface
{

    public static function getSubscribedEvents(): array
    {
        return ['login.success' => 'onLogin'];
    }

    public function onLogin(LoginSuccessEvent $event)
    {

       //@todo
    }
}