<?php

namespace Qerana\Security\Model\Event;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;


class LoginFailSubscriber implements EventSubscriberInterface
{

    public static function getSubscribedEvents(): array
    {
        return ['login.fail' => 'onError'];
    }

    public function onError(LoginFailEvent $event)
    {

       //@todo
    }
}