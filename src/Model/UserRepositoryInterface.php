<?php

namespace Qerana\Security\Model;

interface UserRepositoryInterface
{

    public function findByIdUser(int $id_user): ?UserInterface;

    public function findByUsernameOrFail(string $username): ?UserInterface;

    public function findByUsername(string $username): ?UserInterface;

    public function store(UserInterface $user);

}