<?php

namespace Qerana\Security\Model;

use Ada\AdaRepository;
use Exception;
use Qerana\Security\Model\Exception\UserDoesNotExistsException;

class UserAdaRepository extends AdaRepository implements UserRepositoryInterface
{

    public function __construct()
    {
        parent::__construct('usuarios',UserEntity::class,'id_user');
    }


    /**
     * @throws Exception
     */
    public function findByIdUser(int $id_user): ?UserInterface
    {
        return $this->findOne(['id_user' => $id_user]);
    }

    /**
     * @throws Exception
     */
    public function findByUsernameOrFail(string $username): ?UserInterface
    {

        $user = $this->findByUsername($username);

        if($user === null){
            throw new UserDoesNotExistsException('username:'.$username);
        }

        return $user;

    }

    /**
     * @throws Exception
     */
    public function findByUsername(string $username): ?UserInterface
    {
        return $this->findOne(['username' => $username]);
    }

    /**
     * @throws Exception
     */
    public function store(UserInterface $user)
    {
       $this->save($user);
    }
}