<?php

namespace Qerana\Security\Model;

use JsonSerializable;
use Ada\AdaEntityTrait;

class UserEntity implements UserInterface,JsonSerializable
{

    use AdaEntityTrait;

    private $id_user;
    private $username;
    private $email_user;
    private $password;
    private $status;
    private $user_type;
    private $roles = [];

    public function __construct(array $data = [])
    {
        self::populate($data);
    }


    /**
     * @param string $pass
     */
    public function createPassword(string $pass): void
    {
       $enc_pass = password_hash($pass, PASSWORD_BCRYPT);
       $this->password = $enc_pass;
    }

    /**
     * @param mixed $id_user
     */
    public function setIdUser($id_user): void
    {
        $this->id_user = $id_user;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username): void
    {
        $this->username = $username;
    }

    /**
     * @param mixed $email_user
     */
    public function setEmailUser($email_user): void
    {
        $this->email_user = $email_user;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password): void
    {
        $this->password = $password;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status): void
    {
        $this->status = $status;
    }

    /**
     * @param mixed $user_type
     */
    public function setUserType($user_type): void
    {
        $this->user_type = $user_type;
    }


    public function setRoles(string $roles = null): void
    {
        $this->roles = $roles;
    }


    /**
     * @return mixed
     */
    public function getIdUser()
    {
        return $this->id_user;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return mixed
     */
    public function getEmailUser()
    {
        return $this->email_user;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return mixed
     */
    public function getUserType()
    {
        return $this->user_type;
    }

    /**
     * @return array
     */
    public function getRoles(): array
    {
        return $this->roles;
    }


}