<?php

namespace Qerana\Security\Model\Exception;

use InvalidArgumentException;
use Throwable;

class UserDoesNotExistsException extends InvalidArgumentException
{

    public function __construct($message)
    {
        parent::__construct(sprintf('El usuario con %s no existe!!',$message));
    }
}