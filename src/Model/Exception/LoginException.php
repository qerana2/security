<?php

namespace Qerana\Security\Model\Exception;

use InvalidArgumentException;
use Throwable;

class LoginException extends InvalidArgumentException
{

    public function __construct($message)
    {
        parent::__construct(sprintf('Login fail: %s!!',$message));
    }
}