<?php

namespace Qerana\Security;

use App\Welcome\LoginFormController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;


class QeranaGuard
{


    protected $dispatcher;
    protected $request;

    protected $session;
    protected $remote_addres;
    protected $remote_agent;

    public function __construct(
        Request                  $request,
        EventDispatcherInterface $dispatcher)
    {
        $this->session = new SecureSession();
        $this->request = $request;
        $this->dispatcher = $dispatcher;
        $this->remote_addres = $this->request->getClientIp();
        $this->remote_agent = $this->request->headers->get('user-agent');
    }

    /**
     * check login
     */
    public function check()
    {

        $info = $this->request->headers->get('user-agent');
        if (isset($_SESSION['Q_id_user'], $_SESSION['Q_username'], $_SESSION['Q_login_string'])) {

            return;

        } else {
            $this->goodBye('Authentification required!!');
        }


    }

    /**
     * @param string $message
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    protected function goodBye(string $message = '')
    {

        $this->session->cleanSession();

        $login = new LoginFormController();
        $login->show($this->request);
        die();
    }


}