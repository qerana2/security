<?php

namespace Qerana\Security;

class SecureSession
{

    /**
     * @var false|string
     */
    private $useragent_hash;

    public function __construct()
    {

        $this->useragent_hash = hash('sha256', filter_input(INPUT_SERVER, 'HTTP_USER_AGENT', FILTER_SANITIZE_SPECIAL_CHARS));
        $this->startSession();
    }

    public function startSession()
    {

        if (!isset($_SESSION)) {


            // cookies propagation only via cookies, not url
            ini_set('session.use_only_cookies', 1);

            // entropy file to generate sessions 
            ini_set('session.entropy_file', '/dev/urandom');

            // get cookies parameters
            $cookie_params = session_get_cookie_params();

            session_set_cookie_params(
                $cookie_params['lifetime'], $cookie_params['path'],
                $cookie_params['domain'],
                true, true
            );

            ini_set('session.hash_function', 'sha512');

            // start the secure session, and regenerate for each petition.
            session_name($_ENV['SESSION_NAME']);
            // Hash Useragent to be safe from storing malicious useragent text as session data on server


            session_start();
        }
        // Make sure we have a canary set
        if (!isset($_SESSION['canary'])) {
            // Regenerate & delete old session as well
            session_regenerate_id(true);

            $_SESSION['canary'] = [
                'birth' => time(),
                'ip' => filter_input(INPUT_SERVER, 'REMOTE_ADDR', FILTER_VALIDATE_IP),
                'useragent_hash' => $this->useragent_hash
            ];
        }

        // Regenerate session ID every 5 minutes:
        if ($_SESSION['canary']['birth'] < time() - 300) {
            // Regenerate & delete old session as well
            session_regenerate_id(true);
            $_SESSION['canary']['birth'] = time();
        }

    }

    public function cleanSession()
    {

        $_SESSION = [];
        $params = session_get_cookie_params();

        // delete current cookie
        setcookie(
            session_name(), '', time() - 42000, $params['path'], $params['domain'], $params['secure'], $params['httponly']
        );
        // destruye la sesion
        session_destroy();
    }

}
