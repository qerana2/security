<?php

namespace Qerana\Security;

use Exception;
use App\Welcome\Event\LoginFailEvent;
use App\Welcome\Event\LoginSuccessEvent;
use Qerana\Security\Model\Exception\LoginException;
use Qerana\Security\Model\UserRepositoryInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;

class QeranaLogin extends QeranaGuard
{


    /**
     * @var UserRepositoryInterface
     */
    private $userRepo;
    /**
     * @var Model\UserInterface|null
     */
    private $user;

    private $password;


    /**
     * @param UserRepositoryInterface $userRepo
     * @param Request $request
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(
        UserRepositoryInterface  $userRepo,
        Request                  $request,
        EventDispatcherInterface $dispatcher)
    {
        parent::__construct($request, $dispatcher);
        $this->userRepo = $userRepo;

    }

    /**
     * @throws Exception
     */
    public function handle(string $username, string $password)
    {

        $this->user = $this->userRepo->findByUsernameOrFail($username);
        $this->password = trim($password);

        // check credentials
        $this->checkCredentials();

        // create secure session
        $this->createSesion();

        // fire the event
        $this->dispatcher->dispatch(new LoginSuccessEvent($this->user),LoginSuccessEvent::NAME);

    }

    /**
     *
     */
    private function checkCredentials()
    {
        if ($this->user->getStatus() !== '1') {

            $error = sprintf('Error #LF.001 with %s', $this->user->getUsername());
            $this->triggerFailEvent($error);
            throw new LoginException($error);
        }
        if (!password_verify($this->password, $this->user->getPassword())) {
            $error = sprintf('Error #LF.002 with %s', $this->user->getUsername());
            $this->triggerFailEvent($error);
            throw new LoginException($error);
        }
    }

    /**
     * @param string $error
     */
    private function triggerFailEvent(string $error){
        $this->dispatcher->dispatch(new LoginFailEvent($this->request,$error),LoginFailEvent::NAME);
    }

    /**
     *
     */
    private function createSesion()
    {
        $session = new SecureSession();
        $_SESSION['Q_id_user'] = $this->user->getIdUser();
        $_SESSION['Q_username'] = $this->user->getUsername();
        $_SESSION['Q_logindate'] = date('Y-m-d H:i:s');
        $_SESSION['Q_login_string'] = hash('sha512',
            $this->remote_agent . $this->user->getIdUser()
            . $this->user->getEmailUser() . $this->remote_addres
        );


    }

}